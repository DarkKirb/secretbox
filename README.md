# secretbox

    ⚠️⚠️⚠️ THIS CRATE IS DEPRECATED, USE `crypto_box` INSTEAD! ⚠️⚠️⚠️

Rust-only implementation of NaCl/Libsodium's secure networking protocol used in multiple network protocols

## Built with

- `crypto_box`
- `x25519_dalek`
